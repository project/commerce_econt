<?php

namespace Drupal\commerce_econt;

use Drupal\Component\Serialization\Json;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Provides an EcontAPI SOAP/JSON call implementation.
 */
class EcontJsonAPIService implements EcontJsonAPIServiceInterface {

  use StringTranslationTrait;

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Logger Channel instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a new EcontAPIService instance.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   A logger channel factory instance.
   */
  public function __construct(ClientInterface $http_client, LoggerChannelFactoryInterface $logger_factory) {
    $this->httpClient = $http_client;
    $this->logger = $logger_factory->get('commerce_econt');
  }

  /**
   * {@inheritdoc}
   */
  public function buildRequest(string $endpoint, array $params, int $test_mode = 1, array $credentials = []) {
    $response = [];
    try {
      // Sets the JSON API URL based on the module mode.
      $api_url = ($test_mode) ?
        EcontJsonAPIOptions::DEMO_API_URL :
        EcontJsonAPIOptions::PROD_API_URL;

      // Sets the JSON API BASIC AUTH creds based on the module mode.
      $basic_auth_data =
        ((!$test_mode) && (isset($credentials['username'])) && (isset($credentials['password']))) ?
          $credentials['username'] . ':' . $credentials['password'] :
          EcontJsonAPIOptions::DEMO_USERNAME . ':' . EcontJsonAPIOptions::DEMO_PASSWORD;

      $params = [
        'curl' => [
          CURLOPT_RETURNTRANSFER => 1,
          CURLOPT_SSL_VERIFYPEER => FALSE,
          CURLOPT_SSL_VERIFYHOST => FALSE,
          CURLOPT_HTTPHEADER => FALSE,
          CURLOPT_USERPWD  => $basic_auth_data,

        ],
      ] + $params;

      $request = $this->httpClient->post(
        $api_url . '/' . rtrim($endpoint, '/'),
        ['json' => $params]
      );

      if ($request->getStatusCode() != 200) {
        $error_status_msg = $this->t(
          "Econt API status code error: @code.",
          [
            '@code' => $request->getStatusCode(),
          ]
        );
        throw new \Exception($error_status_msg);
      }

      $response = $request->getBody()->getContents();
    }
    catch (\Exception | GuzzleException $e) {
      $this->logger->error($e->getMessage());
    }

    return Json::decode($response);
  }

}
