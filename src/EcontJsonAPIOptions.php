<?php

namespace Drupal\commerce_econt;

/**
 * This class contains a list of built-in Econt request options.
 *
 * @link https://demo.econt.com/ee/services
 */
final class EcontJsonAPIOptions {

  /**
   * Provides Econt JSON API DEMO Callback URL.
   */
  const DEMO_API_URL = 'https://demo.econt.com/ee/services';

  /**
   * Provides Econt JSON API PROD Callback URL.
   */
  const PROD_API_URL = 'https://ee.econt.com/services/';

  /**
   * Provides Econt JSON API country list.
   */
  const ENDPOINT_COUNTRIES = 'Nomenclatures/NomenclaturesService.getCountries.json';

  /**
   * Provides Econt JSON API cities list by the given country.
   */
  const ENDPOINT_CITIES = 'Nomenclatures/NomenclaturesService.getCities.json';

  /**
   * Provides Econt JSON API endpoint for validating delivery address.
   */
  const ENDPOINT_VALIDATE_ADDRESS = 'Nomenclatures/AddressService.validateAddress.json';

  /**
   * Provides Econt JSON API demo username.
   */
  const DEMO_USERNAME = 'iasp-dev';

  /**
   * Provides Econt JSON API demo password.
   */
  const DEMO_PASSWORD = 'iasp-dev';

}
