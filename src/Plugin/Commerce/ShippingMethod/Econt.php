<?php

namespace Drupal\commerce_econt\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_econt\EcontJsonAPIOptions;
use Drupal\commerce_econt\EcontJsonAPIServiceInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_shipping\ShippingService;
use Drupal\Core\Form\FormStateInterface;
use Drupal\state_machine\WorkflowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Econt shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "econt",
 *   label = @Translation("Econt Shipping"),
 * )
 */
class Econt extends ShippingMethodEcont {

  /**
   * The Econt JSON API service instance.
   *
   * @var \Drupal\commerce_econt\EcontJsonAPIServiceInterface
   */
  protected $econtJSONApi;

  /**
   * Constructs a new Econt object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
   *   The package type manager.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   * @param \Drupal\commerce_econt\EcontJsonAPIServiceInterface $econt_json_api
   *   The Econt JSON API service instance DI.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    PackageTypeManagerInterface $package_type_manager,
    WorkflowManagerInterface $workflow_manager,
    EcontJsonAPIServiceInterface $econt_json_api
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager);

    $this->services['default'] = new ShippingService('default', $this->configuration['rate_label']);

    $this->econtJSONApi = $econt_json_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('plugin.manager.workflow'),
      $container->get('econt_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'rate_label' => '',
      'rate_description' => '',
      'rate_amount' => NULL,
      'services' => ['default'],
      'econt_test_mode' => 1,
      'econt_username' => '',
      'econt_country' => 0,
      'econt_free_delivery_to' => 0,
      'econt_password' => '',
      'econt_emp_name' => '',
      'econt_emp_phone' => '',
      'econt_minimun_rate' => [
        'number' => '',
        'currency_code' => '',
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $econt_api_response = $this->econtJSONApi->buildRequest(
      EcontJsonAPIOptions::ENDPOINT_COUNTRIES,
      [
        'GetCountriesRequest' => '',
      ],
      $this->configuration['econt_test_mode'] ?? 1,
      $this->setUpEcontApiCredentials()
    );

    if (empty($econt_api_response)) {
      $empty_econt_country_data_msg = $this->t('Empty Econt JSON API country data.');
      $this->messenger()->addError($empty_econt_country_data_msg);
      return $form;
    }
    else {
      $form_country_data = [
        0 => $this->t('-- None --'),
      ];
      foreach ($econt_api_response['countries'] as $country) {
        $form_country_data[$country['code3']] = $country['name'];
      }
    }

    $amount = $this->configuration['rate_amount'];
    // A bug in the plugin_select form element causes $amount to be incomplete.
    if (isset($amount) && !isset($amount['number'], $amount['currency_code'])) {
      $amount = NULL;
    }

    $form['econt_test_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Econt Test mode'),
      '#default_value' => $this->configuration['econt_test_mode'],
      '#options' => [
        1 => $this->t('Yes'),
        0 => $this->t('No'),
      ],
      '#required' => TRUE,
    ];

    $form['rate_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Econt label'),
      '#description' => $this->t('Shown to customers during checkout.'),
      '#default_value' => $this->configuration['rate_label'],
      '#required' => TRUE,
    ];

    $form['econt_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Econt Username'),
      '#description' => $this->t('Your Econt API username'),
      '#default_value' => $this->configuration['econt_username'],
    ];

    $form['econt_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Econt password'),
      '#description' => $this->t('Your Econt API password'),
      '#default_value' => $this->configuration['econt_password'],
    ];

    $form['econt_country'] = [
      '#title' => $this->t('Econt Operational country'),
      '#type' => 'select2',
      '#description' => $this->t('Store the Econt operational country for the current store.'),
      '#options' => $form_country_data,
      '#default_value' => $this->configuration['econt_country'],
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'updateFreeDeliveryCity'],
        'wrapper' => 'edit-free-delivery-wrapper',
        'event' => 'change',
        'effect' => 'fade',
      ],
      '#select2' => [
        'width' => 'element',
        'minimumInputLength' => 3
      ],
    ];

    $form['econt_free_delivery_to'] = [
      '#title' => $this->t('Econt Free Delivery City'),
      '#type' => 'select2',
      '#options' => [
        0 => $this->t('-- None --'),
      ],
      '#description' => $this->t('Store the Econt Free Delivery City for the current store(optional).'),
      '#prefix' => '<div id="edit-free-delivery-wrapper">',
      '#suffix' => '</div>',
      '#default_value' => $this->configuration['econt_free_delivery_to'],
      '#validated' => TRUE,
      '#select2' => [
        'width' => 'element',
        'minimumInputLength' => 3
      ],
    ];
    if ($this->configuration['econt_country']) {
      $form['econt_free_delivery_to']['#options'] = $this->getEcontCitiesByCountry($this->configuration['econt_country']);
    }

    $form['econt_emp_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Shipment employee names'),
      '#description' => $this->t('The names of the employee who is responsible for Econt Shipping'),
      '#default_value' => $this->configuration['econt_emp_name'],
      '#required' => TRUE,
    ];

    $form['econt_emp_phone'] = [
      '#type' => 'tel',
      '#title' => $this->t('Shipment employee phone'),
      '#description' => $this->t('The phone of the employee who is responsible for Econt Shipping'),
      '#default_value' => $this->configuration['econt_emp_phone'],
      '#required' => TRUE,
    ];

    $form['econt_minimun_rate'] = [
      '#type' => 'commerce_price',
      '#title' => $this->t('Econt minimum rate'),
      '#default_value' => $this->configuration['econt_minimun_rate'],
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * Builds the requested Econt JSON API credentials.
   *
   * @return array
   *   The defined credentials data array.
   */
  protected function setUpEcontApiCredentials():array {
    // Gets the Econt JSON API country data.
    $econt_api_credentials = [];
    if ($this->configuration['econt_test_mode'] == 0) {
      $econt_api_credentials = [
        'username' => $this->configuration['econt_username'],
        'password' => $this->configuration['econt_password'],
      ];
    }
    return $econt_api_credentials;
  }

  /**
   * Builds an data array by the given Econt JSON API country code.
   *
   * @param string $econt_country_code
   *   The Econt JSON API country code.
   *
   * @return array
   *   The list of cities related to the tagged country.
   */
  protected function getEcontCitiesByCountry(string $econt_country_code = '0'):array {
    $econt_api_response = $this->econtJSONApi->buildRequest(
      EcontJsonAPIOptions::ENDPOINT_CITIES,
      [
        'countryCode' => $econt_country_code,
      ],
      $this->configuration['econt_test_mode'] ?? 1,
      $this->setUpEcontApiCredentials()
    );
    $form_cities_data = [
      0 => $this->t('-- None --'),
    ];
    if (!empty($econt_api_response)) {
      foreach ($econt_api_response['cities'] as $city) {
        $form_cities_data[$city['id']] = $city['name'];
      }
    }

    return $form_cities_data;
  }

  /**
   * The AJAX Callback function, fired on Econt Country change event.
   *
   * @param array $form
   *   The form definition array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The prepared form array element.
   */
  public function updateFreeDeliveryCity(array $form, FormStateInterface $form_state):array {
    $econt_plugin_state_data = $form_state->getValue('plugin');
    $econt_country_code = $econt_plugin_state_data[0]['target_plugin_configuration']['econt']['econt_country'] ?? '0';

    if ($econt_country_code !== '0') {
      $form['plugin']['widget'][0]['target_plugin_configuration']['form']['econt_free_delivery_to']['#options'] = $this->getEcontCitiesByCountry($econt_country_code);
    }

    return $form['plugin']['widget'][0]['target_plugin_configuration']['form']['econt_free_delivery_to'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);
    $store_data = $this->getDefaultStoreData();

    $validate_admin_address_params = [
      'address' => [
        'city' => [
          'country' => [
            'code3' => $values['econt_country'],
          ],
          'name' => $store_data['locality'],
        ],
        'zip' => $store_data['postal_code'],
        "street" => $store_data['address_line1'],
        'num' => $store_data['address_line2'],
      ],
    ];
    $econt_api_response = $this->econtJSONApi->buildRequest(
      EcontJsonAPIOptions::ENDPOINT_VALIDATE_ADDRESS,
      $validate_admin_address_params,
      $this->configuration['econt_test_mode'] ?? 1,
      $this->setUpEcontApiCredentials()
    );

    if (
      empty($econt_api_response['validationStatus']) ||
      $econt_api_response['validationStatus'] != 'normal'
    ) {
      $validate_admin_address_msg = $this->t("Econt JSON API: Invalid default store address.");
      $form_state->setErrorByName('rate_label', $validate_admin_address_msg);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['econt_test_mode'] = $values['econt_test_mode'];
      $this->configuration['rate_label'] = $values['rate_label'];
      $this->configuration['econt_username'] = $values['econt_username'];
      $this->configuration['econt_password'] = $values['econt_password'];
      $this->configuration['econt_country'] = $values['econt_country'];
      $this->configuration['econt_free_delivery_to'] = $values['econt_free_delivery_to'];
      $this->configuration['econt_emp_name'] = $values['econt_emp_name'];
      $this->configuration['econt_emp_phone'] = $values['econt_emp_phone'];
      $this->configuration['econt_minimun_rate'] = $values['econt_minimun_rate'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function calculateRates(ShipmentInterface $shipment) {
    // Rate IDs aren't used in a flat rate scenario because there's always a
    // single rate per plugin, and there's no support for purchasing rates.
    $config = \Drupal::config('commerce_econt.settings');
    $config_mode = ($this->configuration['econt_test_mode']) ? 'demo' : 'live';
    $request_url = $config->get('commerce_econt_settings.' . $config_mode . '_url');

    $payment_gateway = $shipment->getOrder()->get('payment_gateway')->entity->getPlugin();
    $payment_method_name = $payment_gateway->getLabel();
    $is_cod = $payment_method_name == $config->get('commerce_econt_settings.cod_payment_name');

    $requestXml = commerce_econt_calculate_delivery_xml(
      $this->getDefaultStoreData(),
      $this->configuration,
      $this->getShippingData($shipment),
      $is_cod
    );
    $response_data = commerce_econt_send_request_xml($request_url, $requestXml);
    $response_msg = '';
    $amount = $this->configuration['econt_minimun_rate'];

    if (!$response_data['error']) {
      $amount['number'] = $response_data['econt_amount_data'];
    }

    $rate_id = 0;
    $amount = new Price($amount['number'], $amount['currency_code']);

    $rates = [];
    $rates[] = new ShippingRate([
      'shipping_method_id' => $this->parentEntity->id(),
      'service' => $this->services['default'],
      'amount' => $amount,
      'description' => $this->configuration['rate_label'],
    ]);

    return $rates;
  }

}
