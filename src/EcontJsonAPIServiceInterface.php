<?php

namespace Drupal\commerce_econt;

/**
 * Defines the Econt JSON API Service structure.
 */
interface EcontJsonAPIServiceInterface {

  /**
   * Provides an implementation of Econt_JSON_API service calls.
   *
   * @param string $endpoint
   *   The requested endpoint.
   * @param array $params
   *   The requested parameters.
   * @param int $test_mode
   *   The requested env requests mode(1:demo, 0: prod).
   * @param array $credentials
   *   The provided Econt JSON API client credentials.
   *   Required array keys: 'username' and 'password'.
   *
   * @return mixed
   *   The Econt Json API service response.
   */
  public function buildRequest(string $endpoint, array $params, int $test_mode = 1, array $credentials = []);

}
